package flowerShop;

import flowerShop.model.Client;
import flowerShop.model.ClientAnswer;
import flowerShop.model.OffersList;
import flowerShop.model.OrderContext;
import flowerShop.model.Seller;

public class Main {

	public static void main(String[] args) {

		Seller seller = new Seller();
		Client client = new Client("Vasia Pupkin");

		OffersList offersList = new OffersList();

		OrderContext context = new OrderContext(seller, client, offersList);

		seller.greet(client);
		seller.provideFlowerOffers(context);

		while (true) {

			ClientAnswer answer = seller.askClient(client, false);

			if (answer.equals(ClientAnswer.ADD_FLOWER)) {

				seller.addFlowerToBouquet(context);
			}

			if (answer.equals(ClientAnswer.ENOUGH_FLOWERS)) {

				if (context.getCurrentBouquet() == null) {

					System.out.println("You haven't added any flowers. Please try again.");
					continue;
				}

				seller.finishBouquet(context);

			}

			if (answer.equals(ClientAnswer.ENOUGH_BOUQUETS)) {

				if (context.getCurrentBouquet() != null) {

					seller.finishBouquet(context);
				}

				if (context.getOrder().isEmpty()) {

					System.out.println("Your order is empty. Please try again.");
					continue;

				}

				seller.calculatePrice(context);

				return;
			}
		}
	}

}
