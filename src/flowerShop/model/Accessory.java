package flowerShop.model;

public class Accessory {

	private AccessoryType accessoryType;

	public Accessory(AccessoryType accessoryType) {

		this.accessoryType = accessoryType;
	}

	public AccessoryType getAccessoryType() {
		return accessoryType;
	}

}
