package flowerShop.model;

public class AccessoryOffer {

	private int accessoryId;
	private AccessoryType accessoryType;
	private double accessoryPrice;

	public AccessoryOffer(int accessoryId, AccessoryType accessoryType, double accessoryPrice) {

		this.accessoryId = accessoryId;
		this.accessoryType = accessoryType;
		this.accessoryPrice = accessoryPrice;
	}

	public int getAccessoryId() {
		return accessoryId;
	}

	public AccessoryType getAccessoryType() {
		return accessoryType;
	}

	public double getAccessoryPrice() {
		return accessoryPrice;
	}

}
