package flowerShop.model;

public enum AccessoryType {

	RIBBON, WRAPPER, CARD;
}
