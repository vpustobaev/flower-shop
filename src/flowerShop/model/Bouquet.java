package flowerShop.model;

import java.util.ArrayList;

public class Bouquet {

	ArrayList<Flower> flowers = new ArrayList<Flower>();
	ArrayList<Accessory> accessories = new ArrayList<Accessory>();

	public ArrayList<Flower> getFlowers() {
		return flowers;
	}

	public ArrayList<Accessory> getAccessories() {
		return accessories;
	}

	public void addFlower(Flower flower) {

		this.flowers.add(flower);

	}

	public void addAccessory(Accessory accessory) {

		this.accessories.add(accessory);

	}

	public boolean hasAccessory(AccessoryType accessoryType) {

		for (Accessory accessory : accessories) {

			if (accessory.getAccessoryType().equals(accessoryType)) {

				return true;
			}
		}
		return false;
	}

}
