package flowerShop.model;

import java.util.InputMismatchException;
import java.util.Scanner;
import flowerShop.exception.IllegalInputException;

public class Client {

	private static final String ILLEGAL_INPUT = "Illegal input. Please try again";
	private static final String NO_SUCH_FLOWER = "No flower found for the inputted number. Please try again";
	private static final String WRONG_NUMBER = "You entered wrong number of flowers. Please try again";
	private static final String EMPTY_INPUT = "You haven't entered a thing! Please try again";
	private static final String NO_SUCH_ACCESSORY = "No accessory found for the inputted number. Please try again";

	private String name;
	private Scanner el;

	public Client(String name) {

		this.name = name;
	}

	public String getName() {

		return name;
	}

	public ClientAnswer answer(boolean isBouquetFinished) {

		ClientAnswer answer = null;

		String input = null;

		while (true) {

			try {

				el = new Scanner(System.in);
				input = el.nextLine();

			} catch (InputMismatchException e) {

				System.out.println(ILLEGAL_INPUT);
				new IllegalInputException(ILLEGAL_INPUT);
				continue;
			}

			if (input.length() == 0) {

				System.out.println(EMPTY_INPUT);
				continue;
			}

			else {

				switch (input) {

				case "+":

					if (isBouquetFinished) {

						answer = ClientAnswer.ADD_ACCESSORY;

					} else {

						answer = ClientAnswer.ADD_FLOWER;
					}

					break;

				case "-":

					if (isBouquetFinished) {

						answer = ClientAnswer.ENOUGH_ACCESSORIES;

					} else {

						answer = ClientAnswer.ENOUGH_FLOWERS;
					}

					break;

				case "=":

					if (isBouquetFinished) {

						System.out.println(ILLEGAL_INPUT);
						continue;
					}

					answer = ClientAnswer.ENOUGH_BOUQUETS;
					break;

				default:
					System.out.println(ILLEGAL_INPUT);
					continue;
				}

				return answer;
			}
		}
	}

	public AccessoryType chooseAccessoryType(OrderContext context) {

		int input = 0;

		OffersList offersList = context.getOffersList();

		while (true) {

			try {

				el = new Scanner(System.in);
				input = el.nextInt();

				AccessoryOffer accessoryOffer = offersList.getAccessoryOfferById(input);

				if (accessoryOffer == null) {

					System.out.println(NO_SUCH_ACCESSORY);
					continue;
				}

				return accessoryOffer.getAccessoryType();

			} catch (InputMismatchException e) {

				System.out.println(ILLEGAL_INPUT);
				new IllegalInputException(ILLEGAL_INPUT);
				continue;
			}
		}
	}

	public FlowerType chooseFlowerType(OrderContext context) {

		System.out.println("Input the flower's id");

		int input = 0;

		OffersList offersList = context.getOffersList();

		while (true) {

			try {
				el = new Scanner(System.in);
				input = el.nextInt();

				FlowerOffer flowerOffer = offersList.getFlowerOfferById(input);

				if (flowerOffer == null) {

					System.out.println(NO_SUCH_FLOWER);
					continue;
				}

				return flowerOffer.getFlowerType();

			} catch (InputMismatchException e) {

				System.out.println(ILLEGAL_INPUT);
				new IllegalInputException(ILLEGAL_INPUT);
				continue;
			}
		}
	}

	public int chooseQuantityOfFlowers() {

		int input = 0;

		while (true) {

			try {

				el = new Scanner(System.in);
				input = el.nextInt();

				if (input <= 0) {

					System.out.println(WRONG_NUMBER);
					continue;
				}

				return input;

			} catch (InputMismatchException e) {

				System.out.println(ILLEGAL_INPUT);
				new IllegalInputException(ILLEGAL_INPUT);
				continue;
			}
		}
	}

}
