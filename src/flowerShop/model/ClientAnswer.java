package flowerShop.model;

public enum ClientAnswer {

	ADD_FLOWER, ENOUGH_FLOWERS, ENOUGH_BOUQUETS, ADD_ACCESSORY, ENOUGH_ACCESSORIES;

}
