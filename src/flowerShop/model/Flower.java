package flowerShop.model;

public class Flower {

	private FlowerType flowerType;

	public Flower(FlowerType flowerType) {

		this.flowerType = flowerType;
	}

	public FlowerType getFlowerType() {

		return flowerType;
	}

}
