package flowerShop.model;

public class FlowerOffer {

	private int flowerId;
	private FlowerType flowerType;
	private double flowerPrice;

	public FlowerOffer(int flowerId, FlowerType flowerType, double flowerPrice) {

		this.flowerId = flowerId;
		this.flowerType = flowerType;
		this.flowerPrice = flowerPrice;
	}

	public int getFlowerId() {

		return flowerId;
	}

	public FlowerType getFlowerType() {

		return flowerType;
	}

	public double getFlowerPrice() {

		return flowerPrice;
	}

}
