package flowerShop.model;

public enum FlowerType {

	ROSE, LILY, ORCHID, ASTER;
}
