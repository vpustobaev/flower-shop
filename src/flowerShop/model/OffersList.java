package flowerShop.model;

import java.util.ArrayList;

public class OffersList {

	private ArrayList<FlowerOffer> flowerOffers = new ArrayList<FlowerOffer>();
	private ArrayList<AccessoryOffer> accessoryOffers = new ArrayList<AccessoryOffer>();

	public OffersList() {

		flowerOffers.add(new FlowerOffer(1, FlowerType.ORCHID, 10.0));
		flowerOffers.add(new FlowerOffer(2, FlowerType.ROSE, 3.0));
		flowerOffers.add(new FlowerOffer(3, FlowerType.LILY, 7.0));
		flowerOffers.add(new FlowerOffer(4, FlowerType.ASTER, 5.0));

		accessoryOffers.add(new AccessoryOffer(1, AccessoryType.RIBBON, 1.5));
		accessoryOffers.add(new AccessoryOffer(2, AccessoryType.WRAPPER, 1.0));
		accessoryOffers.add(new AccessoryOffer(3, AccessoryType.CARD, 3.0));
	}

	public ArrayList<FlowerOffer> getFlowerOffers() {

		return flowerOffers;
	}

	public ArrayList<AccessoryOffer> getAccessoryOffers() {

		return accessoryOffers;
	}

	public FlowerOffer getFlowerOfferById(int flowerId) {

		for (FlowerOffer flowerOffer : flowerOffers) {

			if (flowerOffer.getFlowerId() == flowerId)
				return flowerOffer;
		}

		return null;
	}

	public FlowerOffer getFlowerOfferByType(FlowerType flowerType) {

		for (FlowerOffer flowerOffer : flowerOffers) {

			if (flowerOffer.getFlowerType() == flowerType)
				return flowerOffer;
		}

		return null;

	}

	public AccessoryOffer getAccessoryOfferById(int accessoryId) {

		for (AccessoryOffer accessoryOffer : accessoryOffers) {

			if (accessoryOffer.getAccessoryId() == accessoryId)
				return accessoryOffer;
		}

		return null;
	}

	public AccessoryOffer getAccessoryOfferByType(AccessoryType accessoryType) {

		for (AccessoryOffer accessoryOffer : accessoryOffers) {

			if (accessoryOffer.getAccessoryType() == accessoryType)
				return accessoryOffer;
		}

		return null;
	}

}
