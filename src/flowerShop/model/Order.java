package flowerShop.model;

import java.util.ArrayList;

public class Order {

	private ArrayList<Bouquet> bouquets = new ArrayList<Bouquet>();

	public void addBouquet(Bouquet bouquet) {

		this.bouquets.add(bouquet);
	}

	public boolean isEmpty() {

		return this.bouquets.isEmpty();
	}

	public ArrayList<Bouquet> getBouquets() {

		return bouquets;
	}

}
