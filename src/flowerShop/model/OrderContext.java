package flowerShop.model;

public class OrderContext {

	private Seller seller;
	private Client client;

	private OffersList offersList = new OffersList();

	private Bouquet currentBouquet;

	private Order order = new Order();

	public OrderContext(Seller seller, Client client, OffersList offersList) {

		if (seller == null) {

			throw new IllegalArgumentException("Seller should not be null");
		}

		if (client == null) {

			throw new IllegalArgumentException("Client should not be null");
		}

		this.seller = seller;
		this.client = client;
	}

	public Seller getSeller() {

		return seller;
	}

	public Client getClient() {

		return client;
	}

	public OffersList getOffersList() {

		return offersList;
	}

	public Bouquet getCurrentBouquet() {

		return currentBouquet;
	}

	public void setCurrentBouquet(Bouquet currentBouquet) {

		this.currentBouquet = currentBouquet;
	}

	public Order getOrder() {

		return order;
	}

}
