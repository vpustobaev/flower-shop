package flowerShop.model;

import java.util.ArrayList;

public class Seller {

	public void greet(Client client) {

		System.out.println("Welcome, dear " + client.getName());
	}

	public void provideFlowerOffers(OrderContext context) {

		OffersList offersList = context.getOffersList();

		ArrayList<FlowerOffer> flowerOffers = offersList.getFlowerOffers();

		System.out.println("Flowers that we have at the moment:");

		for (FlowerOffer flowerOffer : flowerOffers) {

			System.out.println(flowerOffer.getFlowerType() + "(id=" + flowerOffer.getFlowerId() + ")" + " - "
					+ flowerOffer.getFlowerPrice() + " BYN");
		}

		System.out.println();
		System.out.println("If you would like to add a flower, type '+' and then the id number of that flower");
		System.out.println("If you do not need more flowers in a bouquet, type '-'");
		System.out.println("If you do not need more bouquets, type '='");

	}

	public ClientAnswer askClient(Client client, boolean isBouquetFinished) {

		System.out.println("Please make your choice");

		ClientAnswer answer = client.answer(isBouquetFinished);

		return answer;

	}

	public void askForFlowerQuantity(FlowerType flowerType) {

		System.out.println("How many of " + flowerType + " do you want?");

	}

	public void provideAccessoryOffers(OrderContext context) {

		OffersList offersList = context.getOffersList();

		ArrayList<AccessoryOffer> accessoryOffers = offersList.getAccessoryOffers();

		System.out.println("We can offer you some accessories for your bouquet:");

		for (AccessoryOffer accessoryOffer : accessoryOffers) {

			System.out.println(accessoryOffer.getAccessoryType() + "(id=" + accessoryOffer.getAccessoryId() + ")"
					+ " - " + accessoryOffer.getAccessoryPrice() + " BYN");
		}

		System.out.println();
		System.out.println("Do you need an accessory for this bouquet?");
		System.out.println("If do, type '+' and then the id number of that accessory");
		System.out.println("If don't, type '-'");
	}

	public void addFlowerToBouquet(OrderContext context) {

		Client client = context.getClient();
		Bouquet currentBouquet = context.getCurrentBouquet();

		FlowerType flowerType = client.chooseFlowerType(context);

		askForFlowerQuantity(flowerType);

		int quantity = client.chooseQuantityOfFlowers();

		if (currentBouquet == null) {

			currentBouquet = new Bouquet();
			context.setCurrentBouquet(currentBouquet);
			System.out.println("A new bouquet is created");
		}

		for (int i = 1; i <= quantity; i++) {

			Flower flower = new Flower(flowerType);
			currentBouquet.addFlower(flower);
		}

		System.out.println("You have added " + quantity + " " + flowerType + " into a bouquet");

	}

	public void askForAccessory(OrderContext context) {

		while (true) {

			ClientAnswer answer = context.getClient().answer(true);

			if (answer.equals(ClientAnswer.ADD_ACCESSORY)) {
				System.out.println("Input the accessory's id");
				addAccessoryToBouquet(context);
			}

			if (answer.equals(ClientAnswer.ENOUGH_ACCESSORIES))
				break;
		}
	}

	public void addAccessoryToBouquet(OrderContext context) {

		Bouquet currentBouquet = context.getCurrentBouquet();

		AccessoryType accessoryType = context.getClient().chooseAccessoryType(context);

		if (!currentBouquet.hasAccessory(accessoryType)) {

			Accessory accessory = new Accessory(accessoryType);
			currentBouquet.addAccessory(accessory);
			System.out.println("You have added " + accessoryType + " to a bouquet");
			System.out.println("Please make your choice");
		}

		else {

			System.out.println("You can add an accesorry to a bouquet only once. Please try again");
			System.out.println("Please make your choice");
		}
	}

	public void finishBouquet(OrderContext context) {

		provideAccessoryOffers(context);
		askForAccessory(context);
		addBouquetToOrder(context);
	}

	public void addBouquetToOrder(OrderContext context) {

		Bouquet currentBouquet = context.getCurrentBouquet();
		Order order = context.getOrder();
		order.addBouquet(currentBouquet);
		context.setCurrentBouquet(null);
		System.out.println("You have added a bouquet to your order");
	}

	public void calculatePrice(OrderContext context) {

		double price = 0;

		Order order = context.getOrder();

		OffersList offersList = context.getOffersList();

		ArrayList<Bouquet> orderBouquets = order.getBouquets();

		int bouquetsCount = orderBouquets.size();

		for (Bouquet bouquet : orderBouquets) {

			for (Flower flower : bouquet.getFlowers()) {

				double flowerPrice = offersList.getFlowerOfferByType(flower.getFlowerType()).getFlowerPrice();
				price += flowerPrice;
			}

			for (Accessory accessory : bouquet.getAccessories()) {

				double accessoryPrice = offersList.getAccessoryOfferByType(accessory.getAccessoryType())
						.getAccessoryPrice();
				price += accessoryPrice;
			}
		}

		if (bouquetsCount > 2) {

			price *= 0.95;

		} else if (bouquetsCount > 5) {

			price *= 0.90;
		}

		System.out.println("You have total " + bouquetsCount + " number of bouquets in your order");
		System.out.println("The total price for your order is " + price + " BYN");
	}

}
